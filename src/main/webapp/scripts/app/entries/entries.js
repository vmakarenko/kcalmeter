'use strict';

angular.module('kkalMeterApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('entries', {
                parent: 'site',
                url: '/entries',
                data: {
                    authorities: [],
                    pageTitle: 'Entries List!'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entries/entries.html',
                        controller: 'EntriesController'
                    }
                },
                resolve: {}
            })
            .state('entries.edit', {
                parent: 'entries',
                url: '/{id}/edit',
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entries/entries-dialog.html',
                        controller: 'EntriesDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Entry', function(Entry) {
                                return $stateParams.id ? Entry.get({id : $stateParams.id}) : {};
                            }]
                        }
                    }).result.then(function(result) {
                            $state.go('entries', null, { reload: true });
                        }, function() {
                            $state.go('^');
                        })
                }]
            });
    });
