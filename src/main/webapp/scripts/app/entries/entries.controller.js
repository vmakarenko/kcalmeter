'use strict';

angular.module('kkalMeterApp')
    .controller('EntriesController', function ($scope, Entry, User) {
        $scope.entries = [];
        $scope.page = 0;
        $scope.filter = {};
        $scope.loadAll = function () {
            Entry.query({
                dateFrom: moment($scope.filter.dateFrom).isValid() ? moment($scope.filter.dateFrom).format('YYYY-MM-DD') : null,
                dateTo: moment($scope.filter.dateTo).isValid() ? moment($scope.filter.dateTo).format('YYYY-MM-DD') : null,
                timeFrom: $scope.filter.timeFrom != null ? moment($scope.filter.timeFrom).format('HH:mm') : null,
                timeTo: $scope.filter.timeTo != null ? moment($scope.filter.timeTo).format('HH:mm') : null,
                userId: $scope.filter.filterUser != null ? $scope.filter.filterUser.id : null,
                page: $scope.page, per_page: 20
            }, function (result) {
                $scope.entryList = result;
            });
        };

        $scope.loadPage = function (page) {
            $scope.page = page;
            $scope.loadAll();
        };

        $scope.clearFilter = function () {
            $scope.filter.timeFrom = null;
            $scope.filter.timeTo = null;
            $scope.filter.dateFrom = null;
            $scope.filter.dateTo = null;
            $scope.filter.filterUser = null;
            $scope.loadAll();
        };

        $scope.searchFilter = function () {
            $scope.loadAll();
        };


        $scope.clear = function () {
            $scope.entry = {
                id: null, text:null
            };
            if ($scope.editForm) {
                $scope.editForm.$setPristine();
                $scope.editForm.$setUntouched();
            }
        };

        $scope.deleteEntry = function (id) {
            Entry.remove({id: id}, function () {
                $scope.loadAll()
            });
        };

        User.query(function (items) {
            $scope.users = items;
        });

        $scope.clear();
        $scope.clearFilter();
        $scope.loadAll();


    });
