'use strict';

angular.module('kkalMeterApp').controller('EntriesDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Entry', 'User',
        function($scope, $stateParams, $modalInstance, entity, Entry, User) {
            $scope.entry = entity;
            if($scope.entry){
                $scope.entry.entryDate = moment($scope.entry.entryDate).millisecond(0).toDate();
            } else {
                $scope.entry = {};
            }
            if(!$scope.entry.user) {
                $scope.entry.user = {};
            }

            var onSaveSuccess = function (result) {
                $scope.isSaving = false;
                $modalInstance.close(result);
            };

            var onSaveError = function (result) {
                $scope.isSaving = false;
            };

            $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.entry.id != null) {
                    Entry.update($scope.entry, onSaveSuccess, onSaveError);
                } else {
                    Entry.save($scope.entry, onSaveSuccess, onSaveError);
                }
            };

            User.query(function (items) {
                $scope.users = items;
                if(entity != null && entity.userId != null){
                    $scope.entry.user = _.findWhere($scope.users, {id:entity.userId})
                }
            });

            $scope.clear = function() {
                $modalInstance.dismiss('cancel');
            };
        }]);
