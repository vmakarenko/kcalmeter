'use strict';

angular.module('kkalMeterApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('dailyview', {
                parent: 'site',
                url: '/dailyview',
                data: {
                    authorities: [],
                    pageTitle: 'Daily View!'
                },
                views: {
                    'content@': {
                        controller: 'DailyViewController',
                        templateUrl: 'scripts/app/dailyview/dailyview.html'
                    }
                },
                resolve: {}
            })
    });
