'use strict';

angular.module('kkalMeterApp')
    .controller('DailyViewController', function ($scope, Entry, Principal) {
        $scope.currentDate = new Date();

        $scope.entries = [];

        $scope.total = 0;
        Principal.identity().then(function(user) {
            $scope.user = user;
        });

        $scope.page = 0;
        $scope.loadAll = function () {
            Entry.query({date: moment($scope.currentDate).format('YYYY-MM-DD'), page: $scope.page, per_page: 20}, function (result, headers) {
                $scope.entryList = result;
                $scope.total = 0;
                if(result && result.length > 0){
                    result.forEach(function(item){
                        $scope.total += item.calValue;
                    })
                }
            });
        };

        $scope.loadPage = function (page) {
            $scope.page = page;
            $scope.loadAll();
        };

        $scope.loadAll();


        $scope.clear = function () {
            $scope.user = {
                id: null, login: null, firstName: null, lastName: null, email: null,
                activated: null, langKey: null, createdBy: null, createdDate: null,
                lastModifiedBy: null, lastModifiedDate: null, resetDate: null,
                resetKey: null, authorities: null
            };
            $scope.editForm.$setPristine();
            $scope.editForm.$setUntouched();
        };

        $scope.onChangeDate = function(){
            $scope.loadAll();
        };

        $scope.chgDate = function(offset){
            var newDateObj = new Date($scope.currentDate.getTime());
            newDateObj.setDate($scope.currentDate.getDate()+offset);
            $scope.currentDate = newDateObj;
            $scope.onChangeDate();
        }
    });
