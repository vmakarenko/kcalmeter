/* globals $ */
'use strict';

angular.module('kkalMeterApp')
    .directive('kkalMeterAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
