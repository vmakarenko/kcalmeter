/* globals $ */
'use strict';

angular.module('kkalMeterApp')
    .directive('kkalMeterAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
