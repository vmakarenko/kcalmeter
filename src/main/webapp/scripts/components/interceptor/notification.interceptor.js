 'use strict';

angular.module('kkalMeterApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-kkalMeterApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-kkalMeterApp-params')});
                }
                return response;
            }
        };
    });
