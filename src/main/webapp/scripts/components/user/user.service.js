'use strict';

angular.module('kkalMeterApp')
    .factory('User', function ($resource) {
        return $resource('api/users/:login', {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': {method: 'PUT'},
            'delete': {
                method: 'DELETE',
                url: 'api/users/:id'
            }
        });
    });
