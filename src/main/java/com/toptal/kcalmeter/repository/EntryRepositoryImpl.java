package com.toptal.kcalmeter.repository;

import com.toptal.kcalmeter.domain.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * Created by NKolomentsev on 27.07.2016.
 */
public class EntryRepositoryImpl implements EntryRepositoryCustom {

    private final Logger log = LoggerFactory.getLogger(EntryRepositoryImpl.class);
    @Inject
    EntityManager em;

    @Override
    public List<Entry> getByDates(LocalDateTime dateFrom, LocalDateTime dateTo, Long userId) {
        String query = "select * from entries e where 1=1";
        if(userId != null){
            query += " and e.user_id = :userId";
        }
        if(dateFrom != null){
            dateFrom = dateFrom.truncatedTo(ChronoUnit.DAYS);
            query += " and e.datetime >= :dateFrom";
        }
        if(dateTo != null){
            dateTo = dateTo.truncatedTo(ChronoUnit.DAYS);
            query += " and e.datetime < :dateTo";
        }
        Query q = em.createNativeQuery(query, Entry.class);
        if(userId != null){
            q.setParameter("userId", userId);
        }
        if(dateFrom != null){
            q.setParameter("dateFrom", Timestamp.valueOf(dateFrom));
        }
        if(dateTo != null){
            q.setParameter("dateTo", Timestamp.valueOf(dateTo));
        }
        return (List<Entry>)q.getResultList();
    }
}
