package com.toptal.kcalmeter.service;

import com.toptal.kcalmeter.domain.Entry;
import com.toptal.kcalmeter.repository.EntryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class EntryService {

    private final Logger log = LoggerFactory.getLogger(EntryService.class);

    @Inject
    private EntryRepository entryRepository;

    @Transactional(readOnly = true)
    public List<Entry> getByDate(LocalDate date, Long userId) {
        return getByDate(date, date.plusDays(1), null, null, userId);

    }

    @Transactional(readOnly = true)
    public List<Entry> getByDate(LocalDate dateFrom, LocalDate dateTo, LocalTime timeFrom, LocalTime timeTo, Long userId) {
        List<Entry> entryList = entryRepository.getByDates(dateFrom != null ? LocalDateTime.of(dateFrom, LocalTime.MIDNIGHT) : null,
            dateTo != null ? LocalDateTime.of(dateTo, LocalTime.MIDNIGHT) : null, userId);
        if (timeFrom != null || timeTo != null) {
            entryList = entryList.stream().filter(item -> {
                if (timeFrom != null && !item.getEntryDate().toLocalTime().isAfter(timeFrom)) {
                    return false;
                }
                if (timeTo != null && !item.getEntryDate().toLocalTime().isBefore(timeTo)) {
                    return false;
                }
                return true;
            }).collect(Collectors.toList());
        }
        return entryList;
    }
}
