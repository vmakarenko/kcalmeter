/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.toptal.kcalmeter.web.rest.dto;
