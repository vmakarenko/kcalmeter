package com.toptal.kcalmeter.web.rest.dto;

import com.toptal.kcalmeter.domain.Entry;
import com.toptal.kcalmeter.domain.User;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * A DTO representing user entry
 */
public class EntryDto {
    private Long id;

    @Min(0)
    @NotNull
    private Integer calValue;

    private LocalDateTime entryDate = null;

    private String text;

    private Long userId;

    private String userFullName;


    public EntryDto() {
    }

    public EntryDto(Entry entry) {
        this(entry.getId(), entry.getCalValue(), entry.getEntryDate(), entry.getText(),
            entry.getUser() != null ? entry.getUser().getId() : 0,
            entry.getUser() != null ? entry.getUser().getFullName() : "");
    }


    public EntryDto(Long id, Integer calValue, LocalDateTime entryDate, String text, Long userId, String userFullName) {
        this.id = id;
        this.calValue = calValue;
        this.entryDate = entryDate;
        this.text = text;
        this.userId = userId;
        this.userFullName = userFullName;
    }

    @Override
    public String toString() {
        return "EntryDto{" +
            "id=" + id +
            ", calValue=" + calValue +
            ", entryDate=" + entryDate +
            ", text='" + text + '\'' +
            ", userId=" + userId +
            '}';
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCalValue() {
        return calValue;
    }

    public void setCalValue(Integer calValue) {
        this.calValue = calValue;
    }

    public LocalDateTime getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(LocalDateTime entryDate) {
        this.entryDate = entryDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Entry toEntity() {
        Entry result = new Entry();
        result.setUser(new User());
        result.getUser().setId(userId);
        result.setId(id);
        result.setCalValue(calValue);
        result.setEntryDate(entryDate);
        result.setText(text);
        return result;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }
}
